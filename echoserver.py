#!/usr/bin/python
import threading
import socket

class connection(threading.Thread):
    def __init__(self, sock, addr):
    	threading.Thread.__init__(self)
        self.sock = sock
        self.addr = addr
    def run (self):
        while True:
            buffer = self.sock.recv(1024)
            if buffer == "disconnect\r\n":
                self.sock.send("bye")
                break
            elif buffer:
                self.sock.send(buffer)
        self.sock.close()

class echoserver(threading.Thread):
	def __init__(self, port=12119):
		threading.Thread.__init__(self)
		self.running = False
		self._port = port
		self.finish = threading.Event()

	def isRunning(self):
		return self.running

	def port(self):
		return self._port

	def run(self):
		self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self._socket.bind(("0.0.0.0", self.port()))
		self._socket.listen(5)
		while not self.finish.isSet():
			conn, addr = self._socket.accept()
			connection(conn, addr).start()
		self._socket.close()

	def start(self):
		self.running = True
		print "Server is started... at port {}".format(self._port)
		threading.Thread.start(self)
		
	def stop(self):
		self.finish.set()
		self.running = False
		print "Server is closed..." 
		# self.join()

